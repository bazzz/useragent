package useragent

import (
	"testing"
)

func TestWindowsFirefox95Desktop(t *testing.T) {
	input := "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0"
	ua := Parse(input)
	if ua.OS != "Windows" {
		t.Fail()
	}
	if ua.Browser.Name != "Firefox" {
		t.Fail()
	}
	if ua.Browser.Version != "95" {
		t.Fail()
	}
	if ua.Device.Type != "Desktop" {
		t.Fail()
	}
}

func TestAndroidChrome89Mobile(t *testing.T) {
	input := "Mozilla/5.0 (Linux; Android 11; SM-A102U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.72 Mobile Safari/537.36"
	ua := Parse(input)
	if ua.OS != "Android" {
		t.Fail()
	}
	if ua.Browser.Name != "Chrome" {
		t.Fail()
	}
	if ua.Browser.Version != "89" {
		t.Fail()
	}
	if ua.Device.Type != "Mobile" {
		t.Fail()
	}
}

func TestLinuxOpera38Desktop(t *testing.T) {
	input := "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41"
	ua := Parse(input)
	if ua.OS != "Linux" {
		t.Fail()
	}
	if ua.Browser.Name != "Opera" {
		t.Fail()
	}
	if ua.Browser.Version != "38" {
		t.Fail()
	}
	if ua.Device.Type != "Desktop" {
		t.Fail()
	}
}

func TestIosSafari13MobileIphone(t *testing.T) {
	input := "Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1"
	ua := Parse(input)
	if ua.OS != "iOS" {
		t.Fail()
	}
	if ua.Browser.Name != "Safari" {
		t.Fail()
	}
	if ua.Browser.Version != "13" {
		t.Fail()
	}
	if ua.Device.Type != "Mobile" {
		t.Fail()
	}
	if ua.Device.Name != "iPhone" {
		t.Fail()
	}
}

func TestAndroidChrome96MobilePixel(t *testing.T) {
	input := "Mozilla/5.0 (Linux; Android 11; Pixel 5 XL Build/QQ2B.200205.002; wv) AppleWebKit/5310.36 (KHTML, like Gecko) Version/4.0 Chrome/96.0.4664.36 Mobile Safari/5310.36"
	ua := Parse(input)
	if ua.OS != "Android" {
		t.Fail()
	}
	if ua.Browser.Name != "Chrome" {
		t.Fail()
	}
	if ua.Browser.Version != "96" {
		t.Fail()
	}
	if ua.Device.Type != "Mobile" {
		t.Fail()
	}
	if ua.Device.Name != "Pixel 5 XL" {
		t.Fail()
	}
}

func TestAndroidFirefox94NamelessDevice(t *testing.T) {
	input := "Mozilla/5.0 (Android 10; Mobile; rv:94.0) Gecko/94.0 Firefox/94.0"
	ua := Parse(input)
	if ua.OS != "Android" {
		t.Fail()
	}
	if ua.Browser.Name != "Firefox" {
		t.Fail()
	}
	if ua.Browser.Version != "94" {
		t.Fail()
	}
	if ua.Device.Name != "" {
		t.Fail()
	}
}

func TestSafarioniPhone10iOS15_Issue1(t *testing.T) {
	input := "Mozilla/5.0 (iPhone; CPU iPhone OS 15_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/19B74 [FBAN/FBIOS;FBDV/iPhone10,5;FBMD/iPhone;FBSN/iOS;FBSV/15.1;FBSS/3;FBID/phone;FBLC/nl_NL;FBOP/5]"
	ua := Parse(input)
	osExp := "iOS"
	if ua.OS != osExp {
		t.Errorf("OS name %q != %q", ua.OS, osExp)
	}
	browserExp := "Safari"
	if ua.Browser.Name != browserExp {
		t.Errorf("Browser name %q != %q", ua.Browser.Name, browserExp)
	}
	browserVersionExp := "15"
	if ua.Browser.Version != browserVersionExp {
		t.Errorf("Browser version %q != %q", ua.Browser.Version, browserVersionExp)
	}
	deviceExp := "iPhone"
	if ua.Device.Name != deviceExp {
		t.Errorf("Device name %q != %q", ua.Device.Name, deviceExp)
	}
}

func TestSamsungSmartTVTizen_Issue2(t *testing.T) {
	input := "Mozilla/5.0 (SMART-TV; Linux; Tizen 5.5) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/3.0 Chrome/69.0.3497.106 TV Safari/537.36"
	ua := Parse(input)
	osExp := "Tizen"
	if ua.OS != osExp {
		t.Errorf("OS name %q != %q", ua.OS, osExp)
	}
	browserExp := "Samsung Browser"
	if ua.Browser.Name != browserExp {
		t.Errorf("Browser name %q != %q", ua.Browser.Name, browserExp)
	}
	browserVersionExp := "3"
	if ua.Browser.Version != browserVersionExp {
		t.Errorf("Browser version %q != %q", ua.Browser.Version, browserVersionExp)
	}
	deviceExp := "TV"
	if ua.Device.Type != deviceExp {
		t.Errorf("Device type %q != %q", ua.Device.Name, deviceExp)
	}
}
