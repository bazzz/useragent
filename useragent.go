package useragent

import (
	"regexp"
	"strings"
)

type UserAgent struct {
	OS      string
	Browser Browser
	Device  Device
}

type Browser struct {
	Name     string
	Platform string
	Version  string
}

type Device struct {
	Name string
	Type string
}

var tokenizer = regexp.MustCompile(`([A-z]*\/\d+)`)

// Parse reads the provided user agent string input and returns a UserAgent object with the found information.
func Parse(input string) UserAgent {
	openbrace := strings.Index(input, "(")
	closebrace := strings.Index(input, ")")
	system := input[openbrace+1 : closebrace]
	ua := UserAgent{
		OS:      getOS(system),
		Browser: getBrowser(input),
		Device:  getDevice(system, input),
	}
	return ua
}

func getOS(system string) string {
	system = strings.ReplaceAll(system, "; ", " ")
	isLinux := false
	for _, part := range strings.Split(system, " ") {
		switch part {
		case "Windows":
			return "Windows"
		case "Macintosh":
			return "Mac OS"
		case "iPhone", "iPad":
			return "iOS"
		case "Linux":
			isLinux = true
			continue // Linux is usually also Android or Tizen...
		case "FreeBSD":
			return "FreeBSD"
		case "Android":
			return "Android"
		case "Tizen":
			return "Tizen"
		}
	}
	if isLinux { // In case just Linux and not Android or Tizen...
		return "Linux"
	}
	return ""
}

func getBrowser(input string) Browser {
	browser := Browser{}
	brace := strings.Index(input, ")") + 1
	tokens := tokenizer.FindAllString(input[brace:], -1)
	if len(tokens) < 2 {
		return browser
	}
	platform := tokens[0]
	browser.Platform = platform[:strings.Index(platform, "/")]
	if len(tokens) == 2 {
		parts := strings.Split(tokens[1], "/")
		browser.Name = parts[0]
		browser.Version = parts[1]
		return browser
	}
	tags := make(map[string]string)
	for i, token := range tokens {
		if i == 0 {
			continue
		}
		parts := strings.Split(token, "/")
		tags[parts[0]] = parts[1]
	}
	if _, ok := tags["Firefox"]; ok {
		browser.Name = "Firefox"
		browser.Version = tags["Firefox"]
		return browser
	}
	if _, ok := tags["Edg"]; ok {
		browser.Name = "Edge"
		browser.Version = tags["Edg"]
		return browser
	}
	if _, ok := tags["OPR"]; ok {
		browser.Name = "Opera"
		browser.Version = tags["OPR"]
		return browser
	}
	if _, ok := tags["SamsungBrowser"]; ok {
		browser.Name = "Samsung Browser"
		browser.Version = tags["SamsungBrowser"]
		return browser
	}
	if _, ok := tags["Chrome"]; ok {
		browser.Name = "Chrome"
		browser.Version = tags["Chrome"]
		return browser
	}
	if _, ok := tags["Safari"]; ok {
		browser.Name = "Safari"
		browser.Version = tags["Version"]
		return browser
	}
	if _, ok := tags["FBSV"]; ok && browser.Platform == "AppleWebKit" { // Safari on iPhone 10
		browser.Name = "Safari"
		browser.Version = tags["FBSV"]
		return browser
	}
	return browser
}

func getDevice(system string, input string) Device {
	device := Device{
		Type: "Desktop",
	}
	systemParts := strings.ReplaceAll(system, "; ", " ")
	for _, part := range strings.Split(systemParts, " ") {
		switch part {
		case "iPhone":
			device.Name = "iPhone"
			device.Type = "Mobile"
		case "iPad":
			device.Name = "iPad"
			device.Type = "Mobile"
		case "Android":
			name := system[strings.Index(system, ";")+1:]
			name = name[strings.Index(name, ";")+1:]
			if strings.Contains(name, "Build") {
				name = name[:strings.Index(name, "Build")]
			}
			name = strings.Trim(name, " \n\t")
			if strings.HasPrefix(name, "rv") { // Firefox on device with no name.
				name = ""
			}
			device.Name = name
		case "SMART-TV":
			device.Type = "TV"
		}
	}
	if device.Type == "Desktop" && strings.Contains(input, "Mobile") {
		device.Type = "Mobile"
	}
	if device.Type == "Desktop" && strings.Contains(input, " TV ") {
		device.Type = "TV"
	}
	return device
}
